# Sparse Checkout Demo

Demo code for my [article about advantages and disadvantages of Monorepos](https://www.happycoders.eu/java/monorepos-advantages-disadvantages/).

These three GitLab projects belong to the article:
* [Maven Project A](https://gitlab.com/SvenWoltmann/project-a)
* [Maven Project B](https://gitlab.com/SvenWoltmann/project-b)
* [Project A and B merged into one monorepo](https://gitlab.com/SvenWoltmann/sparse-checkout-demo) &larr; this project
